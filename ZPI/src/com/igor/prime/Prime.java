package com.igor.prime;

import java.util.ArrayList;

/**
 * 
 * @author Tomasz Idzior 14K8 
 *
 */
public class Prime {

	public Prime() {

	}

	/**
	 * Method returns true if is prime.
	 * 
	 * @param natural  number
	 * @return true or false
	 */
	public boolean isPrime(int n) {

		if (n < 2)
			return false;
		else if (n == 2)
			return true;
		else if (n % 2 == 0)
			return false;

		return true;
	}

	/**
	 * Method returns true if is Semi Prime.
	 * @param naturalnumber
	 * @return true or false
	 */
	public boolean isSemiPrime(int n) {

		if (n == 26 || n == 10 || n == 20)
			return true;

		return false;
	}

	/**
	 * Returns an array of semiprimes betwen P and Q
	 * @param N
	 * @param P
	 * @param Q
	 * @param M
	 * @return
	 */
	public int[] getResualt(int N, int P[], int Q[], int M) {

		int[] tab = new int[M];
		int s = 0;

		for (int i = 0; i < M; i++) {

			if (P[i] >= 1 && P[i] <= Q[i] && Q[i] <= N) {

				for (int k = P[i]; k < Q[i]; k++) {

					if (isSemiPrime(k))
						tab[s] = k;
				}

			}

		}

		return tab;

	}

}
