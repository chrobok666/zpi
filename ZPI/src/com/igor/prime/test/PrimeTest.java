package com.igor.prime.test;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.igor.prime.Prime;

public class PrimeTest {

	Prime primeTest;

	@Before
	public void before() {
		primeTest = new Prime();
	}

	@Test
	public void isPrime() {

		int prime[] = { 2, 3, 5, 7, 11, 13 };

		for (int i = 0; i < prime.length; i++) {
			assertEquals(true, primeTest.isPrime(i));
		}

	}

	@Test
	public void isSemiPrime() {

		int prime[] = { 4, 6, 9, 10, 14, 15, 21, 22, 25, 26 };

		for (int i = 0; i < prime.length; i++) {
			assertEquals(true, primeTest.isSemiPrime(i));
		}

	}

	@Test
	public void getResulat() {

		int exp[] = { 10, 4, 0 };

		int p[] = { 1, 4, 16 };
		int q[] = { 26, 10, 20 };

		int N = 26;
		int M = 3;

		assertArrayEquals(exp, primeTest.getResualt(N, p, q, M));

	}

}
